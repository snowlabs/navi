import React, {useState, useEffect, useRef} from 'react';
import {Dimensions} from 'react-native';
import {NavigationContainer} from '@react-navigation/native';
import {createStackNavigator, TransitionPresets} from '@react-navigation/stack';
import {createMaterialTopTabNavigator} from '@react-navigation/material-top-tabs';
import RNBootSplash from 'react-native-bootsplash';

import {HomePagerContext} from './src/navigation';
import HomeStack from './src/HomeStack';
import PlayerScreen from './src/PlayerScreen';
import CreationScreen from './src/CreationScreen';
import ProfileScreen from './src/ProfileScreen';
import {HomePagerParams, RootStackParams} from './src/routes';

const HomePager = createMaterialTopTabNavigator<HomePagerParams>();
const HomePagerScreen = () => {
  const [swipeEnabled, setSwipeEnabled] = useState<boolean>(true);
  const {current: ctx} = useRef({setSwipeEnabled});

  return (
    <HomePagerContext.Provider value={ctx}>
      <HomePager.Navigator
        tabBar={_ => false}
        initialRouteName='HomeStack'
        swipeEnabled={swipeEnabled}>
        <HomePager.Screen name='Creation' component={CreationScreen} />
        <HomePager.Screen name='HomeStack' component={HomeStack} />
      </HomePager.Navigator>
    </HomePagerContext.Provider>
  );
};

const RootStack = createStackNavigator<RootStackParams>();
const RootStackScreen = () => (
  <RootStack.Navigator initialRouteName='Pager' headerMode='none'>
    <RootStack.Screen name='Pager' component={HomePagerScreen} />
    <RootStack.Screen
      name='Player'
      component={PlayerScreen}
      options={{
        ...TransitionPresets.ModalSlideFromBottomIOS,
      }}
    />
    <RootStack.Screen
      name='Profile'
      component={ProfileScreen}
      options={{
        ...TransitionPresets.SlideFromRightIOS,
        gestureEnabled: true,
        gestureResponseDistance: {
          horizontal: Dimensions.get('window').width, // default 25
        },
      }}
    />
  </RootStack.Navigator>
);

const App = () => {
  useEffect(() => {
    RNBootSplash.hide({duration: 100});
  });

  return (
    <NavigationContainer>
      <RootStackScreen />
    </NavigationContainer>
  );
};
export default App;
