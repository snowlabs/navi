import React, {useState, useCallback} from 'react';
import {useFocusEffect, RouteProp} from '@react-navigation/core';
import {useSafeArea} from 'react-native-safe-area-context';

import store from '../../storage';
import {HomeStackNav, HomeStackParams} from '../../routes';

import PreviewCardList from './PreviewCardList';
import {InteractionManager, StyleSheet} from 'react-native';
import LinearGradient from 'react-native-linear-gradient';
import C from '../../colors';

interface Props {
  route: RouteProp<HomeStackParams, 'Search'>;
  navigation: HomeStackNav<'Search'>;
  paddingTop: number;
}

const SearchOverlay = ({paddingTop}: Props) => {
  const [{vids, loading}, setVidsState] = useState<{
    vids: VidData[] | null;
    loading: boolean;
  }>({vids: null, loading: true});

  useFocusEffect(
    // To avoid triggering on setState
    useCallback(() => {
      // Fetching the vids is a heavy operation
      const promise = InteractionManager.runAfterInteractions(() => {
        setVidsState(({vids}) => ({vids, loading: true}));
        store
          .get('vids')
          .then(v => {
            if (v) {
              const val = Object.values(JSON.parse(v)) as VidData[];
              setVidsState({vids: val, loading: false});
            }
          })
          .catch(e => {
            console.error(e);
          });
      });
      return () => promise.cancel();
    }, []),
  );

  const safeInsets = useSafeArea();

  return (
    <LinearGradient
      colors={[C.purple3, C.purple4]}
      useAngle={true}
      angle={130}
      style={{...StyleSheet.absoluteFillObject, paddingTop}}>
      <PreviewCardList
        padding={{
          right: 10 + safeInsets.right,
          left: 10 + safeInsets.left,
          bottom: safeInsets.bottom,
        }}
        loading={loading}
        vids={vids === null ? [] : vids}
      />
      <LinearGradient
        colors={[C.search.clear1, C.search.dark1]}
        style={{
          height: 130,
          position: 'absolute',
          bottom: 0,
          left: 0,
          right: 0,
        }}
        pointerEvents='none'
      />
    </LinearGradient>
  );
};

export default SearchOverlay;
