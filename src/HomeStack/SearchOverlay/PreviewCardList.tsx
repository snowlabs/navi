import React from 'react';
import {
  View,
  StyleSheet,
  Text,
  FlatList,
  ViewStyle,
  ActivityIndicator,
} from 'react-native';

import {useNavigation} from '@react-navigation/native';

import {PreviewCardBig} from '../../PreviewCard';
import {HomeStackNav} from '../../routes';
import C from '../../colors';

interface Props {
  style?: ViewStyle;
  loading: boolean;
  vids: VidData[];
  padding?: {top?: number; bottom?: number; left?: number; right?: number};
}

const PreviewCardList = ({style, padding, loading, vids}: Props) => {
  const navigation = useNavigation<HomeStackNav<'Search'>>();

  return (
    <FlatList
      style={{
        paddingTop: padding?.top!,
        paddingLeft: padding?.left!,
        paddingRight: padding?.right!,
        ...style!,
      }}
      ListFooterComponent={() => (
        <View style={{paddingBottom: padding?.bottom!}} />
      )}
      data={vids}
      showsHorizontalScrollIndicator={false}
      keyExtractor={({uri}) => uri}
      numColumns={2}
      // Allows empty component to be `flex 1`
      contentContainerStyle={vids === [] ? {flex: 1} : undefined}
      ListEmptyComponent={() =>
        loading ? (
          <ActivityIndicator
            size='large'
            color={C.search.dark2}
            style={{paddingTop: 30}}
          />
        ) : (
          <View style={styles.listPlaceholder}>
            <Text>Try another search term!</Text>
          </View>
        )
      }
      columnWrapperStyle={styles.listRow}
      renderItem={({item}) => (
        <PreviewCardBig
          upload={item}
          onPress={() => {
            navigation.push('Player', {
              vid: item,
            });
          }}
        />
      )}
    />
  );
};

const styles = StyleSheet.create({
  listPlaceholder: {
    flex: 1,
    justifyContent: 'center',
    alignItems: 'center',
  },
  listRow: {
    justifyContent: 'space-between',
    marginBottom: 10,
  },
});

export default PreviewCardList;
