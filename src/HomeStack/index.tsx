import React, {useMemo, useState, useCallback, useRef} from 'react';
import {View, StyleSheet, TextInput} from 'react-native';
import {useSafeArea} from 'react-native-safe-area-context';
import {createStackNavigator} from '@react-navigation/stack';
import {RouteProp} from '@react-navigation/native';

import LinearGradient from 'react-native-linear-gradient';

import {Spring, useScreenFocusEffect} from '../animation';
import C from '../colors';

import {HomeStackParams, HomePagerParams, HomePagerNav} from '../routes';
import {MockProfile, MockVidPreviews} from '../_mockData';

import _HomeOverlay from './HomeOverlay';
import __SearchOverlay from './SearchOverlay';
import HeaderBar from './HeaderBar';

interface Props {
  route: RouteProp<HomePagerParams, 'HomeStack'>;
  navigation: HomePagerNav<'HomeStack'>;
}

const Stack = createStackNavigator<HomeStackParams>();
const HomeStack = ({navigation}: Props) => {
  const searchInputRef = useRef<TextInput | null>(null);
  const [searchScreenActive, setSearchScreenActive] = useState<boolean>(false);

  const onAvatarPress = useCallback(() => {
    navigation.navigate('Profile', {
      profile: MockProfile,
      uploads: MockVidPreviews,
    });
  }, [navigation]);

  const onSearchScreenFocus = useCallback(() => {
    setSearchScreenActive(true);
    searchInputRef.current?.focus();
  }, []);

  const onHomeScreenFocus = useCallback(() => {
    setSearchScreenActive(false);
    searchInputRef.current?.clear();
    searchInputRef.current?.blur();
  }, []);

  const onHeaderBarFocus = useCallback(() => {
    // preemptively call handler
    onSearchScreenFocus();
    navigation.navigate('HomeStack', {screen: 'Search'});
  }, [navigation, onSearchScreenFocus]);

  const onHeaderBarBackPress = useCallback(() => {
    // preemptively call handler
    onHomeScreenFocus();
    navigation.navigate('HomeStack', {screen: 'Home'});
  }, [navigation, onHomeScreenFocus]);

  // Surefire, yet slightly delayed event
  const HomeOverlay = useScreenFocusEffect(_HomeOverlay, onHomeScreenFocus);
  const _SearchOverlay = useScreenFocusEffect(
    __SearchOverlay,
    onSearchScreenFocus,
  );

  const safeInsets = useSafeArea();
  const SearchOverlay = useCallback(
    props => <_SearchOverlay {...props} paddingTop={safeInsets.top + 60} />,
    [safeInsets.top],
  );

  const transition: {animation: 'spring'; config: object} = {
    animation: 'spring',
    config: {...Spring.DefaultCfg, overshootClamping: true},
  };
  return (
    <LinearGradient
      colors={[C.purple4, C.purple3]}
      useAngle={true}
      angle={130}
      style={StyleSheet.absoluteFill}>
      <Stack.Navigator
        initialRouteName='Home'
        headerMode='none'
        mode='modal'
        keyboardHandlingEnabled={false}>
        <Stack.Screen
          name='Home'
          component={HomeOverlay}
          options={{
            cardStyle: {
              paddingTop: safeInsets.top + 60,
              backgroundColor: 'transparent',
            },
          }}
        />
        <Stack.Screen
          name='Search'
          component={SearchOverlay}
          options={{
            transitionSpec: {
              open: transition,
              close: transition,
            },
            cardStyleInterpolator: ({current}) => {
              return {
                cardStyle: {
                  opacity: current.progress,
                  translateY: current.progress.interpolate({
                    inputRange: [0, 1],
                    outputRange: [40, 0],
                  }),
                },
              };
            },
          }}
        />
      </Stack.Navigator>

      <View style={{paddingTop: 10 + safeInsets.top, ...styles.header}}>
        {useMemo(
          () => (
            <HeaderBar
              inputRef={searchInputRef}
              style={styles.searchBar}
              onFocus={onHeaderBarFocus}
              onBackPress={onHeaderBarBackPress}
              onAvatarPress={onAvatarPress}
              mode={searchScreenActive ? 'Search' : 'Home'}
            />
          ),
          [
            onAvatarPress,
            onHeaderBarBackPress,
            onHeaderBarFocus,
            searchScreenActive,
          ],
        )}
      </View>
    </LinearGradient>
  );
};

const styles = StyleSheet.create({
  header: {
    flexDirection: 'row',
    paddingBottom: 10,
    paddingHorizontal: 10,
    position: 'absolute',
    top: 0,
    left: 0,
    right: 0,
  },
  searchBar: {
    height: 40,
  },
});

export default HomeStack;
