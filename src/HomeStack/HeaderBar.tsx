import React, {MutableRefObject, useMemo} from 'react';
import {
  View,
  TextInput,
  StyleSheet,
  ViewStyle,
  TouchableOpacity,
  TouchableWithoutFeedback,
} from 'react-native';

import Animated from 'react-native-reanimated';

import Icon from 'react-native-vector-icons/MaterialIcons';

import {Spring, useTransition} from '../animation';
import C, {Color} from '../colors';
import {MockProfile} from '../_mockData';

import Avatar from '../Avatar';

interface Props {
  style?: ViewStyle;
  onBackPress?: () => void;
  onFocus?: () => void;
  onAvatarPress?: () => void;
  inputRef?: MutableRefObject<TextInput | null>;
  mode: Mode;
}

const HeaderBar = ({
  style,
  onBackPress,
  onFocus,
  onAvatarPress,
  inputRef,
  mode,
}: Props) => {
  const {avatarStyle, backButtStyle, rootStyle} = useAnimatedStyles(mode);

  return (
    <>
      <Avatar
        diameter={40}
        source={MockProfile.avatarSrc}
        onPress={onAvatarPress}
        style={avatarStyle}
      />
      <Animated.View style={{...rootStyle, ...style}}>
        <Animated.View style={backButtStyle}>
          <TouchableOpacity onPress={onBackPress}>
            <Icon name='arrow-back' size={26} color={C.home.dark1} />
          </TouchableOpacity>
        </Animated.View>
        <View style={styles.inputWrapper}>
          <TextInput
            ref={inputRef}
            onFocus={onFocus}
            style={styles.input}
            placeholder='Search'
            placeholderTextColor={C.home.dark1}
          />
          {mode === 'Home' ? (
            // The focus event on the TextInput is fired with a delay
            <TouchableWithoutFeedback
              onPress={() => {
                onFocus && onFocus();
                inputRef?.current?.focus();
              }}
              style={StyleSheet.absoluteFillObject}>
              <View
                style={{
                  height: '100%',
                  width: '100%',
                  ...StyleSheet.absoluteFillObject,
                  position: 'absolute',
                }}
              />
            </TouchableWithoutFeedback>
          ) : null}
        </View>
        <Icon name='search' size={28} color={C.home.dark1} />
      </Animated.View>
    </>
  );
};

function useAnimatedStyles(
  mode: Mode,
): {backButtStyle: object; avatarStyle: object; rootStyle: object} {
  const valueFor = useTransition(modes, mode, Spring.DefaultCfg);

  const styles = useMemo(
    () => ({
      backButtStyle: {
        marginRight: 8,
        marginLeft: valueFor('backButtLeft'),
        opacity: valueFor('backButtOpacity'),
      },
      avatarStyle: {
        marginRight: 10,
        marginLeft: valueFor('avatarLeft'),
        opacity: valueFor('avatarOpacity'),
      },
      rootStyle: {
        flex: 1,
        borderRadius: 3,
        paddingHorizontal: 8,
        flexDirection: 'row',
        alignItems: 'center',
        backgroundColor: valueFor('color'),
      },
    }),
    [valueFor],
  );

  return styles;
}

const modes = {
  Home: {
    backButtLeft: -(24 + 8),
    backButtOpacity: 0,
    avatarLeft: 0,
    avatarOpacity: 1,
    color: new Color(C.purple3),
  },
  Search: {
    backButtLeft: 0,
    backButtOpacity: 1,
    avatarLeft: -50,
    avatarOpacity: 0,
    color: new Color(C.purple4),
  },
};

export type Mode = keyof typeof modes;

const styles = StyleSheet.create({
  inputWrapper: {
    flex: 1,
  },
  input: {
    fontSize: 18,
    padding: 0,
  },
});

export default HeaderBar;
