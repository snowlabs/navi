import React, {useCallback, useState} from 'react';
import {View, StyleSheet, Text, FlatList} from 'react-native';
import {useFocusEffect, RouteProp} from '@react-navigation/core';

import {useSafeArea} from 'react-native-safe-area-context';

import store from '../storage';
import {PreviewCardBig} from '../PreviewCard';

import {HomeStackParams, HomeStackNav} from '../routes';

interface Props {
  route: RouteProp<HomeStackParams, 'Home'>;
  navigation: HomeStackNav<'Home'>;
}
const HomeOverlay = ({navigation}: Props) => {
  const [vids, setVids] = useState<VidData[] | null>(null);

  useFocusEffect(
    // To avoid triggering on setState
    useCallback(() => {
      store
        .get('vids')
        .then(v => {
          if (v) {
            const val = Object.values(JSON.parse(v)) as VidData[];
            setVids(val);
          }
        })
        .catch(e => {
          console.error(e);
        });
    }, []),
  );

  const safeInsets = useSafeArea();

  return (
    <FlatList
      style={{
        paddingRight: 10 + safeInsets.right,
        paddingLeft: 10 + safeInsets.left,
      }}
      ListFooterComponent={() => (
        <View style={{marginBottom: safeInsets.bottom}} />
      )}
      data={vids === null ? [] : vids}
      showsHorizontalScrollIndicator={false}
      keyExtractor={({uri}) => uri}
      numColumns={2}
      // Allows empty component to be `flex 1`
      contentContainerStyle={vids === null ? {flex: 1} : undefined}
      ListEmptyComponent={() => (
        <View style={styles.listPlaceholder}>
          <Text>Nothing yet!</Text>
        </View>
      )}
      ListHeaderComponent={() => <Text style={styles.heading}>Popular</Text>}
      columnWrapperStyle={styles.listRow}
      renderItem={({item}) => (
        <PreviewCardBig
          upload={item}
          onPress={() => {
            navigation.push('Player', {
              vid: item,
            });
          }}
        />
      )}
    />
  );
};

const styles = StyleSheet.create({
  heading: {
    fontWeight: 'bold',
    fontSize: 26,
    margin: 4,
    marginBottom: 5,
  },
  listPlaceholder: {
    flex: 1,
    justifyContent: 'center',
    alignItems: 'center',
  },
  listRow: {
    justifyContent: 'space-between',
    marginBottom: 10,
  },
});

export default HomeOverlay;
