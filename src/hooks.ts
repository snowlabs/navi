import {useState, useEffect, useCallback} from 'react';
import {Dimensions, Keyboard, KeyboardEvent} from 'react-native';
import {useSafeArea} from 'react-native-safe-area-context';

// NOTE: polyfill for react-native 0.63
export const useWindowDimensions = () => {
  const [dims, setDims] = useState(Dimensions.get('window'));
  const onChange = ({window}) => setDims(window);
  useEffect(() => {
    Dimensions.addEventListener('change', onChange);
    return () => Dimensions.removeEventListener('change', onChange);
  });
  return dims;
};

export const useSafePadding = () => {
  const insets = useSafeArea();
  return {
    paddingTop: insets.top,
    paddingBottom: insets.bottom,
    paddingLeft: insets.left,
    paddingRight: insets.right,
  };
};

export const useKeyboardShown = () => {
  const [shown, setShown] = useState<boolean>(false);
  useKeyboardDidShow(useCallback(() => setShown(true), []));
  useKeyboardDidHide(useCallback(() => setShown(false), []));
  return shown;
};

export const useKeyboardDidShow = (f: (e: KeyboardEvent) => void) => {
  useEffect(() => {
    Keyboard.addListener('keyboardDidShow', f);
    return () => Keyboard.removeListener('keyboardDidShow', f);
  }, [f]);
};
export const useKeyboardDidHide = (f: (e: KeyboardEvent) => void) => {
  useEffect(() => {
    Keyboard.addListener('keyboardDidHide', f);
    return () => Keyboard.removeListener('keyboardDidHide', f);
  }, [f]);
};
