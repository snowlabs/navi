import React, {useEffect, useState, useRef, useCallback} from 'react';
import {StyleSheet} from 'react-native';
import {createStackNavigator} from '@react-navigation/stack';
import {RNCamera} from 'react-native-camera';

import {useWindowDimensions} from '../hooks';
import {CreationStackParams} from '../routes';
import CameraContext, {
  CameraControl,
  RecordingStartCb,
  RecordingEndCb,
  FlashMode,
} from './CameraContext';
import CameraOverlay from './CameraOverlay';
import PlannerOverlay from './PlannerOverlay';
import PublishScreen from './PublishScreen';
import ActionButton, {Props as ActionButtonProps} from './ActionButton';

const Stack = createStackNavigator<CreationStackParams>();
const CreationScreen = () => {
  const {width, height} = useWindowDimensions();
  const ref = useRef<RNCamera>(null);
  const [zoom, setZoom] = useState(0);
  const [flashMode, setFlashMode] = useState<FlashMode>('off');
  const [type, setType] = useState<'back' | 'front'>('back');
  const [poi, setPoi] = useState({x: 0.5, y: 0.5});

  let onRecordingStart = useRef<RecordingStartCb>(() => {});
  let onRecordingEnd = useRef<RecordingEndCb>(() => {});

  const tap = useCallback(
    ({x, y}) => {
      // RNCamera's origin is at topleft of landscape mode, with
      // the home button to the right and 0 <= x,y <= 1.
      // The arguments are in device pixels, so we convert.
      // NOTE: We assume portrait mode, BREAKS OTHERWISE.
      setPoi({
        x: y / height,
        y: 1 - x / width,
      });
    },
    [setPoi, height, width],
  );

  const [actionButtProps, setActionButtonProps] = useState<ActionButtonProps>({
    mode: 'BeginDisabled',
    style: {},
    text: 'Record',
  });

  const ctx = useRef<CameraControl>({
    ref,
    tap,
    flashMode,
    setZoom,
    setActionButtonProps,
    toggleTorch: () => setFlashMode(f => (f === 'off' ? 'torch' : 'off')),
    toggleType: () => setType(t => (t === 'back' ? 'front' : 'back')),
    setOnRecordingStart: f => (onRecordingStart.current = f),
    setOnRecordingEnd: f => (onRecordingEnd.current = f),
  });

  // Update the context when its dependencies change.
  // SetState functions are guaranteed to be stable.
  useEffect(() => {
    ctx.current.ref = ref;
    ctx.current.tap = tap;
    ctx.current.flashMode = flashMode;
  }, [ref, tap, flashMode]);

  const fadeInterpolator = useCallback(
    ({current}) => ({
      cardStyle: {
        opacity: current.progress,
      },
    }),
    [],
  );

  return (
    <RNCamera
      ref={ref}
      type={type}
      zoom={zoom}
      flashMode={flashMode}
      autoFocusPointOfInterest={poi}
      style={styles.root}
      // Wrapping in closure is necessary.
      onRecordingStart={(...x) => onRecordingStart.current?.(...x)}
      onRecordingEnd={(...x) => onRecordingEnd.current?.(...x)}>
      <CameraContext.Provider value={ctx.current}>
        <Stack.Navigator initialRouteName='Planner' headerMode='none'>
          <Stack.Screen
            name='Planner'
            component={PlannerOverlay}
            options={{
              cardStyle: {backgroundColor: 'transparent'},
              cardStyleInterpolator: fadeInterpolator,
            }}
          />
          <Stack.Screen
            name='Camera'
            component={CameraOverlay}
            options={{
              cardStyle: {
                backgroundColor: 'transparent',
              },
              cardStyleInterpolator: fadeInterpolator,
            }}
          />
          <Stack.Screen
            name='Publish'
            component={PublishScreen}
            options={{
              cardStyleInterpolator: fadeInterpolator,
            }}
          />
        </Stack.Navigator>
      </CameraContext.Provider>
      <ActionButton {...actionButtProps} />
    </RNCamera>
  );
};

const styles = StyleSheet.create({
  root: {
    ...StyleSheet.absoluteFillObject,
  },
});

export default CreationScreen;
