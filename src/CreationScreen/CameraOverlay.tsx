import React, {
  useState,
  useRef,
  useContext,
  useCallback,
  useEffect,
  useMemo,
} from 'react';
import {StyleSheet, Text, View} from 'react-native';
import LinearGradient from 'react-native-linear-gradient';
import {RouteProp, useFocusEffect} from '@react-navigation/native';
import {
  State,
  PanGestureHandler,
  PinchGestureHandler,
  TapGestureHandler,
  TouchableWithoutFeedback,
} from 'react-native-gesture-handler';
import Icon from 'react-native-vector-icons/MaterialIcons';

import {useSafePadding} from '../hooks';
import {CreationStackParams, CreationStackNav} from '../routes';
import SectionTitles from '../SectionTitles';
import CameraContext from './CameraContext';
import Animated, {add} from 'react-native-reanimated';
import {Spring, useTransition} from '../animation';
import C from '../colors';

const clamp = (x, l, h) => Math.max(l, Math.min(h, x));

const CounterBadge = ({count}: {count: number}) => (
  <View
    style={{
      width: 30,
      height: 30,
      borderRadius: 30 / 2,
      backgroundColor: C.camera.light1,
      justifyContent: 'center',
      alignItems: 'center',
    }}>
    <Text style={{color: 'white'}}>{count}</Text>
  </View>
);

type SectionClip = {[key: string]: string[]};

const CameraOverlay = ({
  route,
  navigation,
}: {
  route: RouteProp<CreationStackParams, 'Camera'>;
  navigation: CreationStackNav<'Camera'>;
}) => {
  const {decls, title} = route.params;
  const safePad = useSafePadding();
  const camera = useContext(CameraContext);

  let sectionTitles = useRef<SectionTitles>(null);

  const [ind, setInd] = useState(0);
  const [clips, setClips] = useState<SectionClip>(
    Object.fromEntries(decls.map(k => [k.key, []])),
  );

  const [recording, setRecording] = useState(false);

  const end = () => {
    const regex = /^file:\/\//;
    const data: Section.RecordData[] = decls.map(d => ({
      ...d,
      paths: clips[d.key].map(s => s.replace(regex, '')),
    }));

    navigation.navigate('Publish', {
      data: data,
      title: title,
    });
  };

  useEffect(() => {
    camera.setOnRecordingStart(() => {
      setRecording(true);
    });

    camera.setOnRecordingEnd(() => {
      setRecording(false);

      // Go to next section to record
      setInd(i => {
        i = (i + 1) % decls.length; // goes to 0 when past last section
        sectionTitles.current?.focusIndex(i);
        return i;
      });
    });
  }, [camera, setRecording, setInd, decls]);

  const recordPending = useRef(false);
  const toggle = useCallback(() => {
    if (recording) {
      camera.ref.current?.stopRecording();
    } else if (!recordPending.current) {
      recordPending.current = true;
      camera.ref.current
        ?.recordAsync({orientation: 'portrait'})
        .then(data => {
          recordPending.current = false;
          setClips(c => {
            const key = decls[ind].key;
            c[key] = [...c[key], data.uri];
            return c;
          });
        })
        .catch(e => {
          console.error(e);
        });
    }
  }, [recording, camera, decls, ind]);

  const onPanStateChange = useCallback(({nativeEvent: e}) => {
    switch (e.state) {
      case State.BEGAN:
      case State.ACTIVE:
        sectionTitles.current?.onVertScrollStart();
        break;

      case State.END:
        sectionTitles.current?.onVertScrollEnd(-e.translationY);
        break;

      case State.CANCELLED:
        sectionTitles.current?.onVertScrollCancel();
        break;
    }
  }, []);
  const onPanGesture = useCallback(({nativeEvent: e}) => {
    sectionTitles.current?.onVertScrollMove(-e.translationY);
  }, []);

  const prevScale = useRef<number>(1);
  const onPinchStateChange = ({nativeEvent: e}) => {
    if (e.state === State.BEGAN) prevScale.current = e.scale;
  };
  const onPinchGesture = useCallback(
    ({nativeEvent: e}) => {
      const dp = e.scale - prevScale.current;
      if (Math.abs(dp) > 0.01) {
        prevScale.current = e.scale;
        camera.setZoom(z => clamp(z + dp, 0, 1));
      }
    },
    [camera],
  );

  const doubleTap = useRef(null);
  const onTapStateChange = useCallback(
    ({nativeEvent: e}) => {
      if (e.state === State.ACTIVE) camera.tap({x: e.x, y: e.y});
    },
    [camera],
  );
  const onDoubleTapStateChange = useCallback(
    ({nativeEvent: e}) => {
      if (e.state === State.ACTIVE) camera.toggleType();
    },
    [camera],
  );

  useFocusEffect(
    useCallback(() => {
      camera.setActionButtonProps({
        mode: recording ? 'Stop' : 'Record',
        onPress: toggle,
        // style: styles.actionButton,
      });
    }, [recording, camera, toggle]),
  );

  const clipCount = (i: number) => clips[decls[i].key].length;
  const done = Object.values(clips).every(x => x.length > 0);

  const valueFor = useTransition(
    modes,
    done ? 'Created' : 'Creating',
    Spring.DefaultCfg,
  );
  const animatedStyles = useMemo(
    () => ({
      dummyView: {
        width: add(valueFor('width'), valueFor('marginLeft')),
      },
      endButton: {
        width: valueFor('width'),
        height: 40,
        borderRadius: 20,
        backgroundColor: C.green,
        opacity: valueFor('opacity'),
        marginLeft: valueFor('marginLeft'),
      },
    }),
    [valueFor],
  );

  return (
    <PanGestureHandler
      minPointers={1}
      maxPointers={1}
      failOffsetX={[-20, 20]}
      activeOffsetY={[-10, 10]}
      onHandlerStateChange={onPanStateChange}
      onGestureEvent={onPanGesture}>
      <PinchGestureHandler
        onHandlerStateChange={onPinchStateChange}
        onGestureEvent={onPinchGesture}>
        <TapGestureHandler
          waitFor={doubleTap}
          maxDurationMs={100}
          maxDist={5}
          onHandlerStateChange={onTapStateChange}>
          <TapGestureHandler
            ref={doubleTap}
            numberOfTaps={2}
            maxDurationMs={150}
            maxDelayMs={300}
            maxDist={5}
            onHandlerStateChange={onDoubleTapStateChange}>
            <View style={styles.root}>
              <LinearGradient
                colors={[C.camera.dark1, C.camera.clear1]}
                style={styles.topTitle}>
                <View style={styles.sectionTitlesWrapper}>
                  <CounterBadge count={clipCount(ind)} />
                  <SectionTitles
                    ref={sectionTitles}
                    data={decls}
                    style={styles.sectionTitles}
                    height={100}
                    sectionIndex={ind}
                    onSectionChange={i => setInd(i)}
                  />
                </View>
              </LinearGradient>

              <View style={[safePad, styles.topButtons]}>
                <TouchableWithoutFeedback onPress={camera.toggleType}>
                  <Icon
                    name='switch-camera'
                    size={25}
                    color='white'
                    style={styles.topIcon}
                  />
                </TouchableWithoutFeedback>
                <TouchableWithoutFeedback onPress={camera.toggleTorch}>
                  <Icon
                    name={
                      camera.flashMode === 'torch' ? 'flash-on' : 'flash-off'
                    }
                    size={25}
                    color='white'
                    style={styles.topIcon}
                  />
                </TouchableWithoutFeedback>
              </View>

              <View style={styles.bottomButtons}>
                <Animated.View style={animatedStyles.dummyView} />
                <Animated.View style={animatedStyles.endButton}>
                  <TouchableWithoutFeedback
                    onPress={end}
                    style={{
                      height: '100%',
                      width: '100%',
                      justifyContent: 'center',
                      alignItems: 'center',
                    }}>
                    <Icon name='publish' size={25} color='white' />
                  </TouchableWithoutFeedback>
                </Animated.View>
              </View>
            </View>
          </TapGestureHandler>
        </TapGestureHandler>
      </PinchGestureHandler>
    </PanGestureHandler>
  );
};

const modes = {
  Creating: {
    opacity: 0,
    marginLeft: 0,
    width: 0,
  },
  Created: {
    opacity: 1,
    marginLeft: 70,
    width: 40,
  },
};

const styles = StyleSheet.create({
  root: {
    ...StyleSheet.absoluteFillObject,
  },
  topButtons: {
    position: 'absolute',
    flexDirection: 'column',
    top: 100,
    right: 25,
  },
  topIcon: {
    marginBottom: 12,
  },
  bottomButtons: {
    position: 'absolute',
    flexDirection: 'row',
    alignItems: 'center',
    justifyContent: 'center',
    bottom: 62,
    left: 0,
    right: 0,
  },
  topTitle: {
    position: 'absolute',
    height: 170,
    top: 0,
    width: '100%',
    paddingTop: 23,
    paddingLeft: 10,
  },
  sectionTitles: {
    left: 20,
  },
  sectionTitlesWrapper: {
    flexDirection: 'row',
    height: 100,
    alignItems: 'center',
  },
});

export default CameraOverlay;
