import {default as React, useState, useRef, useEffect, RefObject} from 'react';
import {
  StyleSheet,
  TouchableOpacity,
  TouchableWithoutFeedback,
  View,
  Text,
  TextInput,
} from 'react-native';

import DraggableFlatList from 'react-native-draggable-flatlist';
import Icon from 'react-native-vector-icons/MaterialIcons';

import {Spring} from '../../animation';
import C from '../../colors';

const XBUTTON_WIDTH = 27;
function XButton({style, onPress}: {style?: object; onPress?: () => void}) {
  return (
    <TouchableOpacity {...(onPress ? {onPress} : null)}>
      <View
        style={{
          width: XBUTTON_WIDTH,
          height: XBUTTON_WIDTH,
          borderRadius: XBUTTON_WIDTH / 2,
          backgroundColor: C.planner.semi1,
          justifyContent: 'center',
          alignItems: 'center',
          ...style,
        }}>
        <Icon name='close' size={20} color='white' />
      </View>
    </TouchableOpacity>
  );
}

const newId = () =>
  (
    Date.now().toString(36) + Math.random().toString(36).substr(2, 5)
  ).toUpperCase();

const ROW_HEIGHT: number = 50;

const Item = ({
  item,
  drag,
  onChangeText,
  onClose,
  onRef,
  onSubmit,
}: {
  item: Section.Decl;
  drag: any;
  onChangeText: any;
  onClose: any;
  onRef: (ref: RefObject<TextInput>) => void;
  onSubmit: () => void;
}) => {
  const textRef = useRef<TextInput>(null);
  const [isFocused, setIsFocused] = useState(false);

  useEffect(() => onRef(textRef), [onRef, textRef]);

  useEffect(() => {
    textRef.current?.focus();
  }, []);

  return (
    <View
      style={{
        height: ROW_HEIGHT,
        width: '100%',
        flexDirection: 'row',
        alignItems: 'center',
        borderBottomWidth: 1,
        borderBottomColor: C.planner.semi1,
      }}>
      <View style={{flex: 1}}>
        <TextInput
          ref={textRef}
          style={{padding: 0, fontSize: 25, color: 'white'}}
          value={item.title}
          onChangeText={onChangeText}
          onSubmitEditing={onSubmit}
          blurOnSubmit={false}
          placeholder={'Name this section!'}
          placeholderTextColor={C.planner.semi2}
          onFocus={() => setIsFocused(true)}
          onBlur={() => setIsFocused(false)}
        />
      </View>
      {isFocused || (
        <TouchableWithoutFeedback
          delayLongPress={70}
          onLongPress={drag}
          onPress={() => textRef.current?.focus()}
          style={StyleSheet.absoluteFillObject}>
          <View
            style={{
              height: '100%',
              width: '100%',
              ...StyleSheet.absoluteFillObject,
              position: 'absolute',
            }}
          />
        </TouchableWithoutFeedback>
      )}
      <XButton onPress={onClose} style={{marginRight: 2}} />
    </View>
  );
};

export interface Props {
  sections: Section.Decl[];
  onSectionsChange?: (newSections: Section.Decl[]) => void;
}

function SectionList({sections, onSectionsChange}: Readonly<Props>) {
  let refs = useRef<RefObject<TextInput>[]>([]);

  const newSection = () => {
    const newKey = newId();
    onSectionsChange?.([...sections, {title: '', key: newKey}]);
  };

  const newSectionButton = (
    <TouchableOpacity style={{marginBottom: 210}} onPress={newSection}>
      <View style={{flexDirection: 'row', alignItems: 'center', width: '100%'}}>
        <Text
          style={{
            fontSize: 25,
            paddingVertical: 7,
            color: C.planner.semi2,
          }}>
          New Section
        </Text>
        <Icon
          style={{position: 'absolute', right: 3}}
          name='add'
          size={24}
          color={C.planner.semi2}
        />
      </View>
    </TouchableOpacity>
  );

  return (
    <DraggableFlatList
      data={sections}
      renderItem={(props: any) => (
        <Item
          onChangeText={t => {
            let r = [...sections];
            r[props.index] = {...r[props.index], title: t};
            onSectionsChange?.(r);
          }}
          onClose={() => {
            onSectionsChange?.([
              ...sections.slice(0, props.index),
              ...sections.slice(props.index + 1),
            ]);
          }}
          onSubmit={() => {
            if (props.index + 1 < sections.length)
              refs.current[props.index + 1].current?.focus();
            else props.item.textRef?.current?.blur();
          }}
          onRef={r => {
            refs.current[props.index] = r;
          }}
          {...props}
        />
      )}
      animationConfig={Spring.DefaultCfg}
      showsVerticalScrollIndicator={false}
      keyExtractor={sect => sect.key}
      onDragEnd={({data}) => onSectionsChange?.([...data!])}
      ListFooterComponent={newSectionButton}
      keyboardShouldPersistTaps='handled'
      activationDistance={100}
    />
  );
}

export default SectionList;
