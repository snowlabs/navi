import React, {
  useEffect,
  useState,
  useCallback,
  useContext,
  useMemo,
  useRef,
} from 'react';
import {
  Text,
  StyleSheet,
  TextInput,
  View,
  TouchableWithoutFeedback,
} from 'react-native';
import {
  RouteProp,
  useNavigationState,
  useFocusEffect,
} from '@react-navigation/native';
import {SafeAreaView} from 'react-native-safe-area-context';

import CameraContext from '../CameraContext';
import {HomePagerContext} from '../../navigation';
import {CreationStackParams, CreationStackNav} from '../../routes';
import SectionList from './SectionList';
import Animated, {max} from 'react-native-reanimated';
import {useTransition, Spring} from '../../animation';
import C, {Color} from '../../colors';
import {useKeyboardShown} from '../../hooks';

const MIN_SECTIONS: number = 2;

interface Props {
  route: RouteProp<CreationStackParams, 'Planner'>;
  navigation: CreationStackNav<'Planner'>;
}

function PlannerOverlay({navigation}: Props) {
  const [sects, setSects] = useState<Section.Decl[]>([]);
  const [videoTitle, setVideoTitle] = useState<string>('');

  const allowNext = sects.length >= MIN_SECTIONS && videoTitle !== '';

  const onSubmit = useCallback(() => {
    if (allowNext)
      navigation.navigate('Camera', {title: videoTitle, decls: sects});
  }, [allowNext, sects, videoTitle, navigation]);

  const camera = useContext(CameraContext);
  useFocusEffect(
    useCallback(() => {
      camera.setActionButtonProps({
        mode: allowNext ? 'Begin' : 'BeginDisabled',
        onPress: onSubmit,
        text: 'Record',
      });
    }, [allowNext, onSubmit, camera]),
  );

  const kbUp = useKeyboardShown();
  const valueFor = useTransition(
    extraButtModes,
    kbUp && allowNext ? 'Show' : 'Hide',
    Spring.DefaultCfg,
  );
  const extraBeginButtStyles = useMemo<{[key: string]: any}>(
    () => ({
      container: {
        height: 40,
        width: max(0, valueFor('width')),
        opacity: valueFor('opacity'),
        borderRadius: 10,
        backgroundColor: new Color(C.red).alpha(0.7).string(),
        justifyContent: 'center',
        alignItems: 'center',
      },
      text: {
        color: C.planner.light1,
        fontSize: 16,
      },
    }),
    [valueFor],
  );

  // Since some of the screens in CreationScreen act as overlays and
  // overlap each other, we want to not render when overlapping overlays.
  // We also need to disable swiping when moving to Camera.
  const pagerCtx = useContext(HomePagerContext);
  const prevInd = useRef<number>(0);
  const navInd = useNavigationState(st => {
    if (prevInd.current === 2 && st.index === 0) {
      // Reset the planner state, as we've just published.
      // HACK: waiting on redux to be added
      setSects([]);
      setVideoTitle('');
    }

    prevInd.current = st.index;
    return st.index;
  });

  useEffect(() => {
    pagerCtx.setSwipeEnabled?.(navInd === 0);
  }, [pagerCtx, navInd]);

  if (navInd > 0) return <></>;

  return (
    <SafeAreaView style={styles.root}>
      <View style={styles.mainContainer}>
        <View style={styles.videoTitle}>
          <TextInput
            style={styles.videoTitleText}
            placeholder={'Name your video!'}
            placeholderTextColor={C.planner.semi2}
            onChangeText={setVideoTitle}
            value={videoTitle}
          />
          <TouchableWithoutFeedback onPress={onSubmit}>
            <Animated.View style={extraBeginButtStyles.container}>
              <Text style={extraBeginButtStyles.text} numberOfLines={1}>
                Record
              </Text>
            </Animated.View>
          </TouchableWithoutFeedback>
        </View>
        <View style={styles.sectionTitles}>
          <SectionList sections={sects} onSectionsChange={setSects} />
        </View>
      </View>
    </SafeAreaView>
  );
}

const extraButtModes = {
  Hide: {
    opacity: 0,
    width: 0,
  },
  Show: {
    opacity: 1,
    width: 70,
  },
};

const styles = StyleSheet.create({
  root: {
    ...StyleSheet.absoluteFillObject,
    paddingBottom: 0,
    backgroundColor: C.planner.dark1,
  },
  mainContainer: {
    flex: 1,
    padding: 30,
  },
  videoTitle: {
    height: 35,
    width: '100%',
    flexDirection: 'row',
    // justifyContent: 'space-between',
  },
  videoTitleText: {
    flex: 1,
    padding: 0,
    fontSize: 25,
    color: 'white',
  },
  sectionTitles: {
    flex: 1,
    marginTop: 20,
    marginBottom: 140,
  },
});

export default PlannerOverlay;
