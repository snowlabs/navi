import React, {useMemo} from 'react';
import {TouchableWithoutFeedback} from 'react-native';

import Animated, {sub, multiply, max, divide} from 'react-native-reanimated';
import {Spring, useTransition} from '../animation';
import C, {Color} from '../colors';

export interface Props {
  mode: Mode;
  style?: object;
  text?: string;
  onPress?: () => void;
}

const ActionButton = ({mode, style, onPress, text}: Props) => {
  const valueFor = useTransition(modes, mode, Spring.DefaultCfg);

  const styles = useMemo<{[key: string]: object}>(
    () => ({
      container: {
        position: 'absolute',
        alignSelf: 'center',
        bottom: max(0, sub(82, divide(valueFor('height'), 2))),
      },
      root: {
        width: max(0, valueFor('width')),
        height: max(0, valueFor('height')),
        borderColor: valueFor('ringColor'),
        borderWidth: valueFor('ringWidth'),
        borderRadius: valueFor('radius'),
      },
      center: {
        flex: 1,
        alignItems: 'center',
        justifyContent: 'center',
        margin: multiply(valueFor('ringWidth'), 1.5),
        borderRadius: sub(
          valueFor('radius'),
          multiply(valueFor('ringWidth'), 2.5),
        ),
        backgroundColor: valueFor('color'),
      },
      text: {
        padding: 0,
        fontSize: 20,
        color: 'white',
        opacity: max(0, valueFor('textOpacity')),
      },
    }),
    [valueFor],
  );

  // child shouldn't rerender on mode change since it's animated values that mutate
  return useMemo(
    () => (
      <Animated.View style={{...styles.container, ...style}}>
        <TouchableWithoutFeedback onPress={onPress}>
          <Animated.View style={styles.root}>
            <Animated.View style={styles.center}>
              <Animated.Text style={styles.text}>{text}</Animated.Text>
            </Animated.View>
          </Animated.View>
        </TouchableWithoutFeedback>
      </Animated.View>
    ),
    [styles, onPress, style, text],
  );
};

const modes = {
  // on Camera overlay
  Record: {
    width: 75,
    height: 75,
    color: new Color(C.red),
    ringColor: new Color('white'),
    ringWidth: 5,
    radius: 75 / 2,
    textOpacity: 0,
  },
  Stop: {
    width: 75,
    height: 75,
    color: new Color(C.red),
    ringColor: new Color('white'),
    ringWidth: 5,
    radius: 18,
    textOpacity: 0,
  },

  // on Planner overlay
  BeginDisabled: {
    // text: 'Record',
    width: 120,
    height: 75,
    color: new Color(C.purple4).alpha(0.45),
    ringColor: new Color(C.purple4).alpha(0.3),
    ringWidth: 4,
    radius: 24,
    textOpacity: 0.7,
  },

  // on Planner overlay
  Begin: {
    // text: 'Record',
    width: 120,
    height: 75,
    color: new Color(C.red).alpha(0.7),
    ringColor: new Color(C.purple4).alpha(0.45),
    ringWidth: 4,
    radius: 24,
    textOpacity: 0.7,
  },

  // on Publish screen
  Hidden: {
    width: 0,
    height: 0,
    color: new Color(C.purple4).alpha(0),
    ringColor: new Color(C.purple4).alpha(0),
    ringWidth: 4,
    radius: 24,
    textOpacity: 0,
  },
};

export type Mode = keyof typeof modes;
export default ActionButton;
