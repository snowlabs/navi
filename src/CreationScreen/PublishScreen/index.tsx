import React, {FC, useEffect, useState, useContext} from 'react';
import {StyleSheet, Text} from 'react-native';
import {SafeAreaView} from 'react-native-safe-area-context';
import {RouteProp, useFocusEffect} from '@react-navigation/native';

import store from '../../storage';
import {CreationStackParams, CreationStackNav} from '../../routes';
import {processVids} from './fileUtil';
import CameraContext from '../CameraContext';
import MaskedView from '@react-native-community/masked-view';
import Icon from 'react-native-vector-icons/MaterialIcons';
import LinearGradient from 'react-native-linear-gradient';
import Animated, {
  useCode,
  modulo,
  cond,
  set,
  Clock,
  not,
  clockRunning,
  startClock,
  divide,
  sin,
  multiply,
  color,
  interpolate,
} from 'react-native-reanimated';
import {useValue} from '../../animation';
import C from '../../colors';

const GlowingIcon = ({clock}) => {
  const gradAnim = useValue<number>(1);
  useCode(() => {
    const gradPeriod = 1000;
    return [
      cond(not(clockRunning(clock)), startClock(clock)),
      set(gradAnim, sin(divide(clock, gradPeriod))),
    ];
  }, [clock]);

  return (
    <MaskedView maskElement={<Icon name='publish' color='black' size={150} />}>
      <Animated.View
        style={{
          transform: [
            {
              translateX: gradAnim.interpolate({
                inputRange: [-1, 1],
                outputRange: [100, -100],
              }),
            },
            {
              translateY: gradAnim.interpolate({
                inputRange: [-1, 1],
                outputRange: [-100, 100],
              }),
            },
            {scaleX: 3},
            {scaleY: 3},
            {rotateZ: multiply(gradAnim, 5.3)},
          ],
        }}>
        <LinearGradient colors={[C.blue2, C.pink2]}>
          {/* This Icon element does nothing but stretch the
                gradient to the proper size*/}
          <Icon name='publish' color='transparent' size={150} />
        </LinearGradient>
      </Animated.View>
    </MaskedView>
  );
};

const Ellipsis = ({clock}) => {
  const dotsAnim = useValue<number>(0);
  const dotColor = [
    useValue<number>(0),
    useValue<number>(0),
    useValue<number>(0),
  ];

  useCode(() => {
    const dotsPeriod = 2500;
    const stages = [
      [0, 0, 0.3, 1],
      [0, 0.3, 0.6, 1],
      [0, 0.6, 0.9, 1],
    ];
    return [
      set(dotsAnim, divide(modulo(clock, dotsPeriod), dotsPeriod)),
      ...stages.map((s, i) =>
        set(
          dotColor[i],
          color(
            0,
            0,
            0,
            interpolate(dotsAnim, {inputRange: s, outputRange: [0, 0, 1, 1]}),
          ),
        ),
      ),
    ];
  }, []);

  return (
    <>
      <Animated.Text style={{color: dotColor[0]}}>.</Animated.Text>
      <Animated.Text style={{color: dotColor[1]}}>.</Animated.Text>
      <Animated.Text style={{color: dotColor[2]}}>.</Animated.Text>
    </>
  );
};

interface Props {
  route: RouteProp<CreationStackParams, 'Publish'>;
  navigation: CreationStackNav<'Publish'>;
}

const PublishScreen: FC<Props> = ({route, navigation: nav}: Props) => {
  const {title, data} = route.params;
  const [done, setDone] = useState(false);

  const ctx = useContext(CameraContext);
  useFocusEffect(() => {
    ctx.setActionButtonProps({mode: 'Hidden'});
  });

  useEffect(() => {
    // Used to cancel the promise when unmounting.
    let cancel = false;

    processVids(title, data)
      .then(vidData => {
        if (cancel) return;

        store
          .get('vids')
          .then(vids =>
            store.set(
              'vids',
              JSON.stringify({
                ...(vids && JSON.parse(vids)),
                [vidData.uri]: vidData,
              }),
            ),
          )
          .then(_ => {
            setDone(true);
            nav.popToTop();
            nav.goBack();
          })
          .catch(e => {
            console.error(e);
          });
      })
      .catch(e => {
        console.error(e);
      });

    return () => {
      cancel = true;
    };
  }, [data, nav, title]);

  const [clock] = useState(() => new Clock());

  useCode(() => cond(not(clockRunning(clock)), startClock(clock)), [clock]);

  return (
    <SafeAreaView style={styles.root}>
      <GlowingIcon clock={clock} />
      {done ? (
        <Text style={styles.text}>Done!</Text>
      ) : (
        <Text style={styles.text}>
          Publishing
          <Ellipsis clock={clock} />
        </Text>
      )}
    </SafeAreaView>
  );
};

const styles = StyleSheet.create({
  root: {
    ...StyleSheet.absoluteFillObject,
    justifyContent: 'center',
    alignItems: 'center',
    backgroundColor: C.purple4,
  },
  text: {
    marginTop: 15,
    fontSize: 25,
  },
});

export default PublishScreen;
