import {
  RNFFmpeg as FFmpeg,
  RNFFmpegConfig as FFmpegConfig,
  RNFFprobe as FFprobe,
  LogLevel,
  MediaInformation,
} from 'react-native-ffmpeg';
import RNFetchBlob from 'rn-fetch-blob';

FFmpegConfig.setLogLevel(LogLevel.AV_LOG_WARNING);

export const getVidData = (path: string): Promise<MediaInformation> =>
  FFprobe.getMediaInformation(path);

const genIntermediate = async (path: string): Promise<string> => {
  const newPath = path.replace(/\.[^.]*?$/, '.ts');

  // prettier-ignore
  return await FFmpeg.executeWithArguments([
    '-y',
    '-i', path,
    '-c', 'copy',
    '-bsf:v', 'h264_mp4toannexb',
    '-f', 'mpegts',
    newPath,
  ]).then(_ => newPath);
};

/** Catenate several videos and return status. */
const cat = async (uris: string[], outFile: string) => {
  const paths = await Promise.all(uris.map(genIntermediate));

  // prettier-ignore
  return await FFmpeg.executeWithArguments([
    '-y',
    '-f', 'mpegts',
    '-i', `concat:${paths.join('|')}`,
    '-map_metadata', '0',
    '-movflags', 'use_metadata_tags',
    '-metadata:s:v', 'rotate=270',
    '-c', 'copy',
    '-bsf:a', 'aac_adtstoasc',
    outFile,
  ]).then(_ => true);
};

const computeSections = async (
  data: Section.RecordData[],
): Promise<SectionsList> => {
  let sects: SectionsList = [];

  let start = 0;
  for (const {key, title, paths} of data) {
    let length = 0;
    for (const path of paths) {
      const info: MediaInformation = await getVidData(path);
      length += info.duration as number;
    }

    length /= 1000; // ms -> s
    sects.push({key, title, length, start});
    start += length;
  }

  return sects;
};

/** Must be jpeg output */
const genThumbnail = async (
  vidPath: string,
  vidLen: number,
  outFile: string,
) => {
  // prettier-ignore
  return await FFmpeg.executeWithArguments([
    '-i', vidPath,
    '-vframes', '1',
    '-ss', `${vidLen / 2}`, // /2 -> frame at middle of vid
    outFile,
  ]).then(_ => true);
};

const cacheDir = RNFetchBlob.fs.dirs.DocumentDir;
export const processVids = async (
  title: string,
  data: Section.RecordData[],
): Promise<VidData> => {
  const paths: string[] = data
    .map(d => d.paths)
    .reduce((acc, x) => acc.concat(x), []);

  const vidFile = `${cacheDir}/${new Date().getTime()}.mp4`;
  const thumbFile = `${vidFile}.jpg`; // must be jpg

  const uri = `file://${vidFile}`;
  const thumbUri = `file://${thumbFile}`;
  const sects: SectionsList = await computeSections(data);
  const length: number = sects.reduce((acc, sect) => acc + sect.length, 0);

  await cat(paths, vidFile);
  await genThumbnail(vidFile, length, thumbFile);

  return {title, uri, thumbUri, length, sects};
};
