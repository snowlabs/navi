import {
  createContext,
  createRef,
  RefObject,
  Dispatch,
  SetStateAction,
} from 'react';
import {RNCamera} from 'react-native-camera';
import {Props as ActionButtonProps} from './ActionButton';

// Type of callback returned by useState.
type SetState<S> = Dispatch<SetStateAction<S>>;

interface RecordingStartEvent {
  nativeEvent: {
    uri: string;
    videoOrientation: number;
    deviceOrientation: number;
  };
}

export type FlashMode = keyof typeof RNCamera.Constants.FlashMode;
export type RecordingStartCb = (e: RecordingStartEvent) => void;
export type RecordingEndCb = () => void;

export interface CameraControl {
  ref: RefObject<RNCamera>;
  tap: (r: {x: number; y: number}) => void;
  flashMode: FlashMode;
  toggleTorch: () => ReturnType<SetState<FlashMode>>;
  setZoom: SetState<number>;
  setOnRecordingStart: (f: RecordingStartCb) => void;
  setOnRecordingEnd: (f: RecordingEndCb) => void;
  toggleType: () => ReturnType<SetState<'back' | 'front'>>;
  setActionButtonProps: SetState<ActionButtonProps>;
}

const CameraContext = createContext<CameraControl>({
  ref: createRef(),
  tap: () => {},
  flashMode: 'off',
  toggleTorch: () => {},
  setZoom: () => {},
  setOnRecordingStart: () => {},
  setOnRecordingEnd: () => {},
  toggleType: () => {},
  setActionButtonProps: () => {},
});

export default CameraContext;
