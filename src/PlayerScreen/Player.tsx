import React, {
  ReactElement,
  useState,
  useRef,
  useMemo,
  useCallback,
} from 'react';
import {View, ViewStyle, Text, StyleSheet} from 'react-native';
import {
  State,
  PanGestureHandler,
  PanGestureHandlerGestureEvent as GestureEvent,
  PanGestureHandlerStateChangeEvent as StateChange,
} from 'react-native-gesture-handler';
import Video, {OnProgressData} from 'react-native-video';
import {useNavigation, useIsFocused} from '@react-navigation/native';
import {SafeAreaView} from 'react-native-safe-area-context';
import LinearGradient from 'react-native-linear-gradient';

import ProgressBar, {Handles as ProgressBarRef} from './ProgressBar';
import SideButtons from './SideButtons';
import SectionTitles from '../SectionTitles';

import C from '../colors';
import {MockProfile, MockVidPreviews} from '../_mockData';

interface Props {
  style?: ViewStyle;
  vidData: VidData;
}

function Player({
  style,
  vidData: {title, sects, uri, length},
}: Props): ReactElement {
  const navigation = useNavigation();
  const isFocused = useIsFocused();
  const sectionProportions = useMemo(
    () => sects.map(({length: l}) => l / length),
    [sects, length],
  );
  const [section, setSection] = useState<number>(0);
  const [_paused, setPaused] = useState<boolean>(false);
  const paused = _paused || !isFocused;
  const video = useRef<Video>(null);
  const sectionTitles = useRef<SectionTitles>(null);
  const scrolling = useRef<boolean>(false);
  const progBar = useRef<ProgressBarRef>(null);

  const onHandlerStateChange = ({nativeEvent: e}: StateChange) => {
    switch (e.state) {
      case State.BEGAN:
      case State.ACTIVE:
        scrolling.current = true;
        sectionTitles.current?.onVertScrollStart();
        break;

      case State.END:
        scrolling.current = false;
        sectionTitles.current?.onVertScrollEnd(-e.translationY);
        break;

      case State.CANCELLED:
      case State.FAILED:
        scrolling.current = false;
        sectionTitles.current?.onVertScrollCancel();
        break;
    }
  };

  const onGestureEvent = ({nativeEvent: e}: GestureEvent) => {
    sectionTitles.current?.onVertScrollMove(-e.translationY);
  };

  const onAvatarPress = useCallback(() => {
    navigation.navigate('Profile', {
      profile: MockProfile,
      uploads: MockVidPreviews,
    });
  }, [navigation]);

  const onPausePress = useCallback(() => {
    setPaused(p => !p); // toggle
  }, []);

  const onBackPress = useCallback(() => {
    navigation.goBack();
  }, [navigation]);

  const onSectionChange = (s: number) => {
    if (s !== section) {
      setSection(s);
    }
  };

  const onSectionHighlight = (s: number) => {
    video.current?.seek(sects[s].start);
    progBar.current?.setProgress(sects[s].start / length);
  };

  function onProgress({currentTime: t}: OnProgressData) {
    progBar.current?.setProgress(t / length);

    if (!scrolling.current && t >= sects[section + 1]?.start) {
      setSection(section + 1);
    }
  }

  function onEnd() {
    setPaused(true);
    setSection(0);
    onSectionHighlight(0);
    progBar.current?.setProgress(0);
  }

  return (
    <PanGestureHandler
      onHandlerStateChange={onHandlerStateChange}
      onGestureEvent={onGestureEvent}>
      <View style={style}>
        <Video
          ref={video}
          progressUpdateInterval={16.6} // 60fps progress bar
          source={{uri: uri}}
          resizeMode={'cover'}
          onProgress={onProgress}
          onEnd={onEnd}
          paused={paused}
          style={styles.video}
        />
        <LinearGradient
          colors={[C.player.dark2, C.player.clear1]}
          style={styles.gradient}
        />
        <SafeAreaView style={styles.mainContainer}>
          <Text style={styles.title}>{title}</Text>
          <View style={styles.sectionsContainer}>
            <SectionTitles
              ref={sectionTitles}
              data={sects}
              height={styles.sectionsContainer.height}
              sectionIndex={section}
              onSectionChange={onSectionChange}
              onSectionHighlight={onSectionHighlight}
            />
          </View>
          <SideButtons
            style={styles.sideButtons}
            paused={_paused}
            onAvatarPress={onAvatarPress}
            onPausePress={onPausePress}
            onBackPress={onBackPress}
          />
          <ProgressBar
            ref={progBar}
            proportions={sectionProportions}
            color={C.player.light1}
            style={styles.progBar}
            segmentStyles={segmentStyles}
          />
        </SafeAreaView>
      </View>
    </PanGestureHandler>
  );
}

const styles = StyleSheet.create({
  video: {
    ...StyleSheet.absoluteFillObject,
    backgroundColor: 'black',
  },
  gradient: {
    ...StyleSheet.absoluteFillObject,
    height: 340,
  },
  mainContainer: {
    flex: 1,
    marginTop: 20,
    marginLeft: 20,
  },
  title: {
    color: C.player.light1,
    fontSize: 16,
  },
  sectionsContainer: {
    height: 100,
  },
  progBar: {
    position: 'absolute',
    top: 20,
    bottom: 20,
    right: 8,
    width: 8,
    backgroundColor: C.player.dark1,
  },
  sideButtons: {
    position: 'absolute',
    top: 20,
    bottom: 20,
    right: 16,
    paddingRight: 8,
  },
});

const segmentStyles = StyleSheet.create({
  top: {
    borderTopRightRadius: 10,
    borderTopLeftRadius: 10,
    marginBottom: 3,
  },
  mid: {
    marginTop: 3,
    marginBottom: 3,
  },
  bot: {
    borderBottomRightRadius: 10,
    borderBottomLeftRadius: 10,
    marginTop: 3,
  },
});

export default Player;
