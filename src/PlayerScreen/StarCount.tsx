import React from 'react';
import {StyleSheet, Text, View, ViewStyle} from 'react-native';
import Icon from 'react-native-vector-icons/MaterialIcons';
import C from '../colors';

const CIRCLE_WIDTH = 34;

const styles = StyleSheet.create({
  container: {
    alignItems: 'center',
  },
  starCircle: {
    width: CIRCLE_WIDTH,
    height: CIRCLE_WIDTH,
    borderRadius: CIRCLE_WIDTH / 2,
    backgroundColor: C.player.light2,
    alignItems: 'center',
    justifyContent: 'center',
    marginBottom: 10,
  },
});

const StarCount = ({count, style}: {count: string; style: ViewStyle}) => (
  <View style={{...style, ...styles.container}}>
    <View style={styles.starCircle}>
      <Icon name='star' size={25} color={C.yellow} />
    </View>
    <Text style={{fontSize: 16, color: C.player.light1}}>{count}</Text>
  </View>
);

export default StarCount;
