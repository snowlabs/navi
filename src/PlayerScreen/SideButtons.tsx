import React from 'react';

import {
  View,
  ViewStyle,
  StyleSheet,
  TouchableHighlight,
  StyleProp,
} from 'react-native';
import Icon from 'react-native-vector-icons/MaterialIcons';

import Avatar from '../Avatar';
import StarCount from './StarCount';

import C from '../colors';
import {MockProfile} from '../_mockData';

const SideButtons = ({
  style,
  paused,
  onAvatarPress,
  onPausePress,
  onBackPress,
}: {
  style?: ViewStyle;
  paused: boolean;
  onAvatarPress?: () => void;
  onPausePress?: () => void;
  onBackPress?: () => void;
}) => (
  <View style={[style, styles.container]}>
    <View style={styles.topButtons}>
      <Avatar
        source={MockProfile.avatarSrc}
        diameter={52}
        style={styles.top}
        onPress={onAvatarPress}
      />
      {/* FIXME: dummy video starcount */}
      <StarCount style={styles.top} count={'300'} />
      <DarkButton
        style={[styles.button, styles.top]}
        onPress={onPausePress}
        icon={paused ? 'play-arrow' : 'pause'}
      />
    </View>
    <DarkButton
      style={styles.button}
      onPress={onBackPress}
      icon='keyboard-arrow-down'
    />
  </View>
);

const DarkButton = ({
  onPress,
  style,
  icon,
}: {
  onPress?: () => void;
  style?: StyleProp<ViewStyle>;
  icon: string;
}) => (
  <TouchableHighlight
    style={style}
    underlayColor={C.player.dark2}
    onPress={onPress}>
    <Icon name={icon} color={C.player.light1} size={30} />
  </TouchableHighlight>
);

const styles = StyleSheet.create({
  container: {
    flexDirection: 'column',
    justifyContent: 'space-between',
    alignItems: 'center',
  },
  topButtons: {
    alignItems: 'center',
  },
  top: {
    marginBottom: 10,
  },
  button: {
    backgroundColor: C.player.dark1,
    width: 36,
    height: 36,
    borderRadius: 3,
    alignItems: 'center',
    justifyContent: 'center',
  },
});

export default SideButtons;
