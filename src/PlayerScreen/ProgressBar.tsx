import React, {
  RefForwardingComponent,
  memo,
  useRef,
  forwardRef,
  useImperativeHandle,
} from 'react';
import {View, ViewStyle} from 'react-native';
import MaskedView from '@react-native-community/masked-view';
import Animated from 'react-native-reanimated';

const {Value, sub} = Animated;

const Mask = memo(
  ({
    proportions,
    segmentStyles,
  }: {
    proportions: number[];
    segmentStyles?: {top: ViewStyle; mid: ViewStyle; bot: ViewStyle};
  }) => {
    const total = proportions.reduce((a, b) => a + b, 0);
    proportions.reverse();
    return (
      <>
        {proportions.map((p, i) => (
          <View
            key={i}
            style={{
              ...segmentStyles?.[
                i === 0 ? 'top' : i === proportions.length - 1 ? 'bot' : 'mid'
              ],
              backgroundColor: 'black',
              flex: p / total,
            }}
          />
        ))}
      </>
    );
  },
);

export interface Props {
  proportions: number[];
  style?: ViewStyle;
  segmentStyles?: {top: ViewStyle; mid: ViewStyle; bot: ViewStyle};
  color: string;
}

export interface Handles {
  setProgress: (x: number) => void;
}

const ProgressBar: RefForwardingComponent<Handles, Props> = (
  {proportions, style, segmentStyles, color},
  ref,
) => {
  const {current: progress} = useRef<Animated.Value<number>>(new Value(0));

  useImperativeHandle(ref, () => ({
    setProgress: (x: number) => {
      progress.setValue(x);
    },
  }));

  return (
    <MaskedView
      style={style}
      maskElement={
        <Mask proportions={proportions} segmentStyles={segmentStyles} />
      }>
      <Animated.View style={{flex: sub(1, progress)}} />
      <Animated.View
        style={{
          backgroundColor: color,
          flex: progress,
        }}
      />
    </MaskedView>
  );
};

export default forwardRef(ProgressBar);
