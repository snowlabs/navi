import React from 'react';
import {View, StyleSheet} from 'react-native';
import {RouteProp} from '@react-navigation/native';

import {RootStackParams, RootStackNav} from '../routes';
import Player from './Player';

interface Props {
  route: RouteProp<RootStackParams, 'Player'>;
  navigation: RootStackNav<'Player'>;
}

const PlayerScreen = ({route, navigation: _nav}: Props) => {
  const {vid} = route.params;

  return (
    // SafeArea handled in Player
    <View style={styles.root}>
      <Player style={styles.video} vidData={vid} />
    </View>
  );
};

const styles = StyleSheet.create({
  root: {
    ...StyleSheet.absoluteFillObject,
  },
  video: {
    flex: 1,
  },
});

export default PlayerScreen;
