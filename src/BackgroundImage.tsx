import React from 'react';
import {StyleSheet, View, Image} from 'react-native';

export interface Props {
  style?: object;
  source: object;
  resizeMode?: 'cover' | 'contain' | 'stretch' | 'repeat' | 'center';
  overlayColor?: string;
}

function BackgroundImage(props: React.PropsWithChildren<Props>) {
  return (
    <View style={props.style}>
      {/*
        without this wrapper View, the Image does not actually
        Absolute-ly fill the parent, it obeys the parents padding
        and is weirdly offset
      */}
      <View style={StyleSheet.absoluteFillObject}>
        <Image
          source={props.source}
          style={{
            height: '100%',
            width: '100%',
            ...StyleSheet.absoluteFillObject,
          }}
          resizeMode={props.resizeMode}
        />
        {props.overlayColor ? (
          <View
            style={{
              backgroundColor: props.overlayColor,
              ...StyleSheet.absoluteFillObject,
            }}
          />
        ) : null}
      </View>
      {props.children}
    </View>
  );
}

export default BackgroundImage;
