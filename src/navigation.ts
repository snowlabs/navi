import {createContext, Dispatch, SetStateAction} from 'react';

interface HomePagerOptions {
  setSwipeEnabled?: Dispatch<SetStateAction<boolean>>;
}

export const HomePagerContext = createContext<HomePagerOptions>({});
