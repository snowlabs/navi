import ASFactory from '@react-native-community/async-storage';
import LegacyStorage from '@react-native-community/async-storage-backend-legacy';

export type Model = {
  // FIXME: ensure there is way of avoiding JSON
  vids: string /* {[key: string]: VidData} JSON */;
};

const legacyStorage = new LegacyStorage();
const storage = ASFactory.create<Model>(legacyStorage, {
  logger: ({action, key, value}) => {
    console.log(`store: ${action} ${key} ${JSON.stringify(value)}`);
  },
  errorHandler: e => {
    console.log(`store: ERR ${JSON.stringify(e)}`);
  },
});
export default storage;
