import React from 'react';
import {Text, Image, ViewStyle, TouchableOpacity} from 'react-native';

import LinearGradient from 'react-native-linear-gradient';
import {useSpring, Spring, useValue} from './animation';
import C from './colors';
import Animated from 'react-native-reanimated';

export interface Props {
  style?: ViewStyle;
  upload: VidPreview;
  onPress?: () => void;
}

interface _Props extends Props {
  dims: {
    width: number;
    height: number;
  };
  gradientHeight: number;
  fontSize: number;
}

const PreviewCard = (props: _Props) => {
  const opacity = useSpring(Spring.DefaultCfg, 0, useValue(1));

  return (
    <Animated.View style={{opacity, ...props.style}}>
      <TouchableOpacity activeOpacity={0.8} onPress={props.onPress}>
        <Image
          style={props.dims}
          source={{uri: props.upload.thumbUri}}
          resizeMode={'cover'}
        />
        <LinearGradient
          colors={[C.card.semi2, C.card.semi1]}
          style={{
            position: 'absolute',
            width: '100%',
            height: props.gradientHeight,
            paddingHorizontal: 5,
            paddingTop: 3,
          }}>
          <Text
            style={{
              fontSize: props.fontSize,
              color: 'white',
            }}>
            {props.upload.title}
          </Text>
        </LinearGradient>
      </TouchableOpacity>
    </Animated.View>
  );
};

export const PreviewCardSmall = (props: Props) => (
  <PreviewCard
    {...props}
    dims={{width: 110, height: 150}}
    gradientHeight={60}
    fontSize={14}
  />
);

export const PreviewCardBig = (props: Props) => (
  <PreviewCard
    {...props}
    dims={{width: 164, height: 220}}
    gradientHeight={100}
    fontSize={18}
  />
);
