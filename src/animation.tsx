import React, {useRef, useCallback, useEffect, ComponentType} from 'react';
import {
  Node,
  Value,
  Clock,
  clockRunning,
  onChange,
  cond,
  startClock,
  stopClock,
  spring,
  set,
  useCode,
  add,
  color,
  eq,
} from 'react-native-reanimated';
import {useFocusEffect} from '@react-navigation/core';
import {Color} from './colors';

export type Atomic = number | string | boolean;

export interface SpringConfig {
  damping: number | Node<number>;
  mass: number | Node<number>;
  stiffness: number | Node<number>;
  overshootClamping: number | boolean | Node<number>;
  restSpeedThreshold: number | Node<number>;
  restDisplacementThreshold: number | Node<number>;
  toValue: Value<number>;
}

export namespace Spring {
  export const DefaultCfg: SpringConfig = {
    damping: 5,
    mass: 0.2,
    stiffness: 100,
    overshootClamping: false,
    restSpeedThreshold: 0.001,
    restDisplacementThreshold: 0.001,
    toValue: new Value(1),
  };
}

export function useValue<T extends Atomic>(v: T): Value<T> {
  const val = useRef<Value<T> | undefined>();
  if (val.current === undefined) val.current = new Value(v);
  return val.current;
}

export function useSpring(
  cfg: SpringConfig,
  init: number,
  target: Value<number>,
): Value<number> {
  const position = useValue(init);
  useCode(() => {
    cfg.toValue = target;

    const state = {
      finished: new Value(0),
      position,
      velocity: new Value(0),
      time: new Value(0),
    };
    const clock = new Clock();

    const changed = new Value(0);

    return [
      cond(eq(changed, 0), startClock(clock)),
      onChange(target, set(changed, add(changed, 1))),
      onChange(position, set(changed, add(changed, 1))),
      onChange(
        changed,
        cond(clockRunning(clock), set(cfg.toValue, target), [
          set(state.finished, 0),
          set(state.velocity, 0),
          set(state.time, 0),
          set(cfg.toValue, target),
          startClock(clock),
        ]),
      ),
      spring(clock, state, cfg),
      cond(state.finished, stopClock(clock)),
    ];
  }, [position, cfg, target]);

  return position;
}

type Effect = (props: any) => void | (() => void);
function FocusHandlingScreen({
  onFocus,
  Screen,
  props,
}: {
  onFocus: Effect;
  Screen: ComponentType<any>;
  props: any;
}) {
  useFocusEffect(useCallback(() => onFocus(props), [onFocus, props]));
  return <Screen {...props} />;
}

export function useScreenFocusEffect(
  screen: ComponentType<any>,
  onFocus: Effect,
) {
  return useCallback(
    props => (
      <FocusHandlingScreen Screen={screen} onFocus={onFocus} props={props} />
    ),
    [screen, onFocus],
  );
}

function EffectHandlingScreen({
  effect,
  Screen,
  props,
}: {
  effect: Effect;
  Screen: ComponentType<any>;
  props: any;
}) {
  useEffect(() => effect(props), [effect, props]);
  return <Screen {...props} />;
}

export function useScreenEffect(screen: ComponentType<any>, effect: Effect) {
  return useCallback(
    props => (
      <EffectHandlingScreen Screen={screen} effect={effect} props={props} />
    ),
    [screen, effect],
  );
}

class TransitionColor {
  r: Value<number>;
  g: Value<number>;
  b: Value<number>;
  a: Value<number>;

  constructor(c: Color) {
    this.r = new Value(c.r);
    this.g = new Value(c.g);
    this.b = new Value(c.b);
    this.a = new Value(c.a);
  }
}

function usePersistent<T>(init: () => T): T {
  const ref = useRef<T | undefined>();
  if (ref.current === undefined) ref.current = init();
  return ref.current;
}

const genValues = <T, M extends keyof T>(modes: T, initMode: M): any =>
  Object.fromEntries(
    Object.entries(modes[initMode]).map(([k, v]) =>
      v instanceof Color
        ? [k, new TransitionColor(v)]
        : [k, new Value(v as Atomic)],
    ),
  );

export function useTransition<T, M extends keyof T, K extends keyof T[M]>(
  modes: T,
  mode: M,
  springCfg: SpringConfig,
): (key: K) => Node<number> {
  const {flip, flop} = usePersistent<{flip: any; flop: any}>(() => ({
    flip: genValues(modes, mode),
    flop: genValues(modes, mode),
  }));
  const target = useValue<number>(0);
  const progress = useSpring(springCfg, 0, target);
  const setValues = useCallback(
    (obj, mode: M) =>
      Object.entries(obj).forEach(([k, v]) => {
        const modeObj = modes[mode];
        v instanceof TransitionColor
          ? Object.entries(v).forEach(([kk, vv]) =>
              (vv as Value<any>).setValue(modeObj[k][kk]),
            )
          : v instanceof Value
          ? (v as Value<any>).setValue(modeObj[k])
          : // this impossible noop case actually is
            // reached when hot-reload is triggered
            null;
      }),
    [modes],
  );

  const reverse = useRef(false);
  useEffect(() => {
    console.log('New mode:', mode);
    if (reverse.current) {
      setValues(flip, mode);
      target.setValue(0);
      progress.setValue(1);
    } else {
      setValues(flop, mode);
      target.setValue(1);
      progress.setValue(0);
    }
    reverse.current = !reverse.current;
  }, [flip, flop, mode, progress, setValues, target]);

  return useCallback(
    (key: K): Node<number> => {
      if (flip[key] instanceof Value) {
        return progress.interpolate({
          inputRange: [0, 1],
          outputRange: [flip[key], flop[key]],
        });
      } else {
        const {r: ar, g: ag, b: ab, a: aa} = flop[key];
        const {r: br, g: bg, b: bb, a: ba} = flip[key];
        return color(
          progress.interpolate({
            inputRange: [-1, 0, 1, 2],
            outputRange: [ar, br, ar, br],
          }),
          progress.interpolate({
            inputRange: [-1, 0, 1, 2],
            outputRange: [ag, bg, ag, bg],
          }),
          progress.interpolate({
            inputRange: [-1, 0, 1, 2],
            outputRange: [ab, bb, ab, bb],
          }),
          progress.interpolate({
            inputRange: [-1, 0, 1, 2],
            outputRange: [aa, ba, aa, ba],
          }),
        );
      }
    },
    [flip, flop, progress],
  );
}
