import React, {memo} from 'react';
import {
  Image,
  ImageSourcePropType,
  TouchableOpacity,
  ViewStyle,
} from 'react-native';
import Animated from 'react-native-reanimated';

interface Props {
  diameter?: number;
  source: ImageSourcePropType;
  onPress?: () => void;
  style?: ViewStyle;
}

const Avatar = ({source, diameter = 36, style, onPress}: Props) => {
  const img = (
    <Image
      source={source}
      style={{
        width: diameter,
        height: diameter,
        borderRadius: diameter / 2,
      }}
    />
  );

  return onPress ? (
    <Animated.View style={style}>
      <TouchableOpacity onPress={onPress}>{img}</TouchableOpacity>
    </Animated.View>
  ) : (
    <Animated.View style={style}>{img}</Animated.View>
  );
};
export default memo(Avatar);
