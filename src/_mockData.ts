export const MockProfile: UserProfile = {
  name: 'Frank Gomes',
  tag: 'franku',
  location: 'Montreal, QC',
  bio: 'I make nice pizza and\nhashbrowns.\nNew upload every thursday!',
  stars: '5.2k',
  avatarSrc: require('../assets/dummyAvatar.jpeg'),
  bannerSrc: require('../assets/dummyBanner.jpeg'),
  socials: [
    {platform: 'snap', url: 'https://www.snapchat.com/add/djkhaled305'},
    {platform: 'yt', url: 'https://www.youtube.com/user/penguinz0'},
  ],
};

export const MockVidPreviews: VidPreview[] = [
  {title: 'Making Halloween sushi', thumbUri: require('../assets/thumb1.jpeg')},
  {title: 'Baking bacon bits', thumbUri: require('../assets/thumb2.jpeg')},
  {
    title: 'Cooking cookie crumbles',
    thumbUri: require('../assets/thumb3.jpeg'),
  },
  {
    title: 'Preparing poodle plumps',
    thumbUri: require('../assets/thumb4.jpeg'),
  },
  {
    title: 'Homemade hungry hotdogs',
    thumbUri: require('../assets/thumb5.jpeg'),
  },
  {title: 'Raising raisin bread', thumbUri: require('../assets/thumb6.jpeg')},
  {title: 'Making Halloween sushi', thumbUri: require('../assets/thumb1.jpeg')},
];
