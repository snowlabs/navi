import React from 'react';
import {Text, FlatList, ViewStyle} from 'react-native';
import C from './colors';

export interface Props {
  data: Section.Decl[];
  sectionIndex: number;
  style?: ViewStyle;
  height: number;
  onSectionChange: (curSection: number) => void;
  onSectionHighlight?: (index: number) => void;
}

interface State {
  selectedSection: number;
}

export default class SectionTitles extends React.Component<Props, State> {
  _titleList?: FlatList<Section.Decl>;
  vertPadding: number;
  startOffset: number = 0;

  constructor(props: Props) {
    super(props);

    this.state = {
      selectedSection: props.sectionIndex,
    };

    this.vertPadding = props.height / 2 - this.itemHeight / 2;
  }

  componentDidUpdate = (prevProps: Props) => {
    const newIndex = this.props.sectionIndex;
    if (newIndex !== prevProps.sectionIndex) {
      this.focusIndex(newIndex);
      this.setState({selectedSection: newIndex});
    }
  };

  focusIndex(index: number) {
    this._titleList!.scrollToIndex({
      index: index,
      viewPosition: 0.5,
    });
  }

  nearestIndex(offset: number) {
    return Math.min(
      this.props.data.length - 1,
      Math.max(0, Math.round((this.startOffset + offset) / this.itemHeight)),
    );
  }

  scrollSensivity: number = 0.4;
  onVertScrollStart() {
    this.startOffset = this.props.sectionIndex * this.itemHeight;
  }

  onVertScrollMove(dy: number) {
    dy = dy * this.scrollSensivity;

    this._titleList!.scrollToOffset({
      offset: this.startOffset + dy,
      animated: false,
    });

    let index: number = this.nearestIndex(dy);
    this.setState((st, props) => {
      if (index === st.selectedSection) {
        return null; // cancel render
      } else {
        props.onSectionHighlight?.(index);
        return {selectedSection: index};
      }
    });
  }

  onVertScrollEnd(dy: number) {
    dy = dy * this.scrollSensivity;

    // Trigger callback if section is swiped and changed.
    // Otherwise, snap back to the section.
    const index = this.nearestIndex(dy);
    if (index !== this.props.sectionIndex) this.props.onSectionChange(index);
    else this.focusIndex(index); // snap back
  }

  onVertScrollCancel() {
    this.setState((_, props) => {
      this.focusIndex(props.sectionIndex);
      return {selectedSection: props.sectionIndex};
    });
  }

  itemHeight: number = 34;
  render = () => (
    <FlatList
      ref={(ref: FlatList<Section.Decl>) => {
        this._titleList = ref;
      }}
      data={this.props.data}
      renderItem={({item: {title}, index}) => (
        <Text
          style={{
            color:
              index === this.state.selectedSection ? 'white' : C.titles.semi1,
            fontWeight: 'bold',
            fontSize: 26,
            height: this.itemHeight,
          }}>
          {title}
        </Text>
      )}
      getItemLayout={(data, index) => ({
        length: this.itemHeight,
        offset: this.itemHeight * index + this.vertPadding,
        index: index,
      })}
      initialScrollIndex={this.props.sectionIndex}
      style={{...this.props.style, height: this.props.height}}
      contentContainerStyle={{paddingVertical: this.vertPadding}}
      snapToOffsets={(() => {
        let r: number[] = [];
        for (let i = 0; i < this.props.data.length; i++) {
          r.push(this.itemHeight * i + this.vertPadding);
        }
        return r;
      })()}
      // TODO: find a way to get rid of the bounce/arc/feedback thing on android
      bounces={false}
    />
  );
}
