import React from 'react';
import {View, ViewStyle, Image, Linking, TouchableOpacity} from 'react-native';

import C from '../colors';

const SOCIAL_PROPS = {
  snap: {
    bg: C.social.snapchatYellow,
    icon: require('../../assets/snapIcon.png'),
  },
  yt: {
    bg: C.social.youtubeRed,
    icon: require('../../assets/ytIcon.png'),
  },
};

const Socials = ({
  socials,
  itemStyle,
}: {
  socials: SocialLink[];
  itemStyle: ViewStyle;
}) => (
  <View style={{flexDirection: 'row'}}>
    {socials.map((social, i) => {
      const sprops = SOCIAL_PROPS[social.platform];
      return (
        <View
          key={i}
          style={{
            width: 38,
            height: 38,
            borderRadius: 38 / 2,
            backgroundColor: sprops.bg,
            alignItems: 'center',
            justifyContent: 'center',
            ...itemStyle,
          }}>
          {/* FIXME: handle invalid URL */}
          <TouchableOpacity onPress={() => Linking.openURL(social.url)}>
            <Image
              source={sprops.icon}
              resizeMode={'contain'}
              style={{width: 23, height: 23}}
            />
          </TouchableOpacity>
        </View>
      );
    })}
  </View>
);

export default Socials;
