import React from 'react';
import {StyleSheet, Text, View} from 'react-native';
import Icon from 'react-native-vector-icons/MaterialIcons';

import C from '../colors';

const styles = StyleSheet.create({
  container: {
    flexDirection: 'row',
    height: 38,
    padding: 4,
    backgroundColor: C.profile.light1,
    borderRadius: 50,
  },
  starCircle: {
    width: 30,
    height: 30,
    borderRadius: 30 / 2,
    backgroundColor: C.yellow,
    alignItems: 'center',
    justifyContent: 'center',
  },
});

const StarCount = ({count}: {count: string}) => (
  <View style={styles.container}>
    <View style={styles.starCircle}>
      <Icon name='star' size={25} />
    </View>
    <Text style={{marginHorizontal: 5, fontSize: 18, alignSelf: 'center'}}>
      {count}
    </Text>
  </View>
);

export default StarCount;
