import React, {useState, useCallback} from 'react';
import {StyleSheet, Text, View, FlatList} from 'react-native';
import {useSafeArea} from 'react-native-safe-area-context';
import LinearGradient from 'react-native-linear-gradient';
import {RouteProp} from '@react-navigation/native';
import {useFocusEffect} from '@react-navigation/core';
import Socials from './Socials';
import StarCount from './StarCount';
import BackgroundImage from '../BackgroundImage';
import {RootStackParams, RootStackNav} from '../routes';
import Avatar from '../Avatar';
import store from '../storage';
import {PreviewCardSmall} from '../PreviewCard';
import C from '../colors';

export interface Props {
  route: RouteProp<RootStackParams, 'Profile'>;
  navigation: RootStackNav<'Profile'>;
}

const AVATAR_WIDTH = 95;
function ProfileHeader({profile}: {profile: UserProfile}) {
  const safeInsets = useSafeArea();

  return (
    <>
      <BackgroundImage
        source={profile.bannerSrc}
        style={{
          flexDirection: 'row',
          paddingTop: safeInsets.top,
          paddingRight: safeInsets.right,
          paddingLeft: safeInsets.left,
        }}
        overlayColor={C.profile.dark2}
        resizeMode={'cover'}>
        <View style={{padding: 13}}>
          <Avatar source={profile.avatarSrc} diameter={AVATAR_WIDTH} />
          <Text style={styles.grey}>{profile.location}</Text>
        </View>
        <View style={{marginTop: 10, marginBottom: 15}}>
          <Text style={styles.heading}>{profile.name}</Text>
          <Text style={styles.grey}>{'@' + profile.tag}</Text>
          <Text style={styles.paragraph}>{profile.bio}</Text>
        </View>
      </BackgroundImage>
      <View
        style={{
          paddingTop: 15,
          paddingRight: 10 + safeInsets.right,
          paddingLeft: 10 + safeInsets.left,
        }}>
        <View style={styles.headerButtons}>
          <StarCount count={profile.stars} />
          <Socials socials={profile.socials} itemStyle={{marginLeft: 10}} />
        </View>
        <Text style={styles.listTitle}>Spotlights</Text>
      </View>
    </>
  );
}

function ProfileScreen({route, navigation}: Props) {
  const safeInsets = useSafeArea();
  const {profile} = route.params;
  const [uploads, setUploads] = useState<VidData[]>([]);

  useFocusEffect(
    // To avoid triggering on setState
    useCallback(() => {
      store
        .get('vids')
        .then(v => {
          if (v) {
            const val = Object.values(JSON.parse(v)) as VidData[];
            setUploads(val);
          }
        })
        .catch(e => {
          console.error(e);
        });
    }, []),
  );

  return (
    <LinearGradient
      colors={[C.purple4, C.purple3]}
      useAngle={true}
      angle={135}
      style={StyleSheet.absoluteFill}>
      <FlatList
        numColumns={3}
        data={uploads}
        keyExtractor={({thumbUri}) => thumbUri}
        // Allows empty component to be `flex 1`
        contentContainerStyle={uploads.length ? undefined : {flex: 1}}
        ListEmptyComponent={() => (
          <View style={styles.listPlaceholder}>
            <Text>Nothing yet!</Text>
          </View>
        )}
        ListHeaderComponent={() => <ProfileHeader profile={profile} />}
        columnWrapperStyle={{
          ...styles.listRow,
          paddingRight: 10 + safeInsets.right,
          paddingLeft: 10 + safeInsets.left,
          paddingBottom: safeInsets.bottom,
        }}
        renderItem={({item}) => (
          <PreviewCardSmall
            upload={item}
            onPress={() => {
              navigation.push('Player', {
                vid: item,
              });
            }}
          />
        )}
      />
      <LinearGradient
        colors={[C.profile.clear1, C.profile.dark1]}
        style={{
          height: 130,
          position: 'absolute',
          bottom: 0,
          left: 0,
          right: 0,
        }}
        pointerEvents='none'
      />
    </LinearGradient>
  );
}

const styles = StyleSheet.create({
  listRow: {
    justifyContent: 'space-between',
    marginBottom: 6,
  },
  listPlaceholder: {
    flex: 1,
    justifyContent: 'center',
    alignItems: 'center',
  },
  heading: {
    fontSize: 25,
    fontWeight: 'bold',
    color: 'white',
  },
  paragraph: {
    color: 'white',
    fontSize: 17,
  },
  grey: {color: C.profile.light1},
  headerButtons: {
    flexDirection: 'row',
    justifyContent: 'space-between',
    marginBottom: 10,
  },
  listTitle: {
    marginBottom: 6,
    fontSize: 18,
    color: 'black',
  },
  shadowGradient: {
    position: 'absolute',
    bottom: 0,
    left: 0,
    right: 0,
    height: 135,
  },
});

export default ProfileScreen;
