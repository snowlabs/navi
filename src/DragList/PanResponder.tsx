import {
  PanResponder as PResponder,
  GestureResponderEvent,
  PanResponderGestureState,
  NativeTouchEvent,
} from 'react-native';

export interface Handlers {
  shouldCapture: () => boolean;
  onVertScrollStart: (e: NativeTouchEvent) => void;
  onVertScrollMove: (e: NativeTouchEvent, dy: number) => void;
  onVertScrollEnd: (dy: number) => void;
}

export default function PanResponder(handlers: Handlers) {
  return PResponder.create({
    // Ask to be the responder:
    onStartShouldSetPanResponder: () => true,
    onStartShouldSetPanResponderCapture: () => true,
    onMoveShouldSetPanResponder: handlers.shouldCapture, // () => true,
    onMoveShouldSetPanResponderCapture: handlers.shouldCapture,

    onPanResponderTerminationRequest: () => true,
    onShouldBlockNativeResponder: () => true,

    onPanResponderGrant: (
      evt: GestureResponderEvent,
      _stat: PanResponderGestureState,
    ) => {
      // The gesture has started. Show visual feedback so the user knows
      // what is happening! gestureState.d{x,y} will be set to zero now

      handlers.onVertScrollStart(evt.nativeEvent);
    },
    onPanResponderMove: (
      evt: GestureResponderEvent,
      stat: PanResponderGestureState,
    ) => {
      // The most recent move distance is gestureState.move{X,Y}
      // The accumulated gesture distance since becoming responder is
      // gestureState.d{x,y}

      handlers.onVertScrollMove(evt.nativeEvent, stat.dy);
    },
    onPanResponderRelease: (
      evt: GestureResponderEvent,
      stat: PanResponderGestureState,
    ) => {
      // The user has released all touches while this view is the
      // responder. This typically means a gesture has succeeded

      handlers.onVertScrollEnd(stat.dy);
    },

    onPanResponderTerminate: (
      _evt: GestureResponderEvent,
      _stat: PanResponderGestureState,
    ) => {
      // Another component has become the responder, so this gesture
      // should be cancelled
    },
  });
}
