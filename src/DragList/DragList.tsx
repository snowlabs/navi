import React from 'react';
import {
  FlatList,
  ListRenderItemInfo,
  TouchableWithoutFeedback,
} from 'react-native';

export interface Props<I> {
  data: I[];
  renderItem: (info: ListRenderItemInfo<I>) => any;
  style?: object;
  delayStartDrag?: number;
}

function DragList<I>(props: Props<I>) {
  const onItemLongPress = () => {
    console.log('long press!');
  };

  return (
    <FlatList
      style={props.style}
      data={props.data}
      renderItem={info => (
        <TouchableWithoutFeedback
          delayLongPress={props.delayStartDrag || 150}
          onLongPress={onItemLongPress}>
          {props.renderItem(info)}
        </TouchableWithoutFeedback>
      )}
    />
  );
}

export default DragList;
