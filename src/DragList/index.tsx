import {default as React, useState, useMemo, useRef} from 'react';
import {
  StyleSheet,
  View,
  FlatList,
  ListRenderItemInfo,
  TouchableWithoutFeedback,
  LayoutAnimation,
  UIManager,
  Platform,
  LayoutChangeEvent,
} from 'react-native';
import PanResponder from './PanResponder';

if (
  Platform.OS === 'android' &&
  UIManager.setLayoutAnimationEnabledExperimental
) {
  UIManager.setLayoutAnimationEnabledExperimental(true);
}

export interface Props<I> {
  data: I[];
  renderItem: (info: ListRenderItemInfo<I>) => any;
  itemHeight: number;
  style?: object;
  delayStartDrag?: number;
  onSwapItems: (oldIndex: number, newIndex: number) => void;
}

function DragList<I>(props: Props<I>) {
  const flatList = useRef(null);
  const [dragging, setDragging] = useState<number | null>(null);
  const [draggingOfs, setDraggingOfs] = useState<number | undefined>(undefined);

  const onLayout = ({nativeEvent: {layout}}: LayoutChangeEvent) => {
    console.log(layout);
  };

  const getItemLayout = (data: I[] | null, index: number) => {
    return {
      length: props.itemHeight,
      offset:
        props.itemHeight * index +
        (dragging !== null && dragging! === index ? draggingOfs! : 0),
      index,
    };
  };

  const onItemLongPress = (index: number) => {
    setDragging(index);
  };

  const swapThreshold: number = 1.0;
  const [midEvent, setMidEvent] = useState(null);
  const panHandlers = useMemo(() => {
    let initPageY = 0;
    let initTouchY = 0;

    const getDy = evt => evt.pageY - initPageY;

    const startDrag = evt => {
      initTouchY =
        dragging! * props.itemHeight + evt.locationY - props.itemHeight / 5;
      initPageY = evt.pageY;
      console.log('initTouchY: ', initTouchY);
    };

    if (midEvent != null) startDrag(midEvent);

    return {
      shouldCapture: () => dragging != null,
      onVertScrollStart: evt => {
        startDrag(evt);
      },
      onVertScrollMove: (evt, _dy: number) => {
        const dy = getDy(evt);

        const touchY = initTouchY + dy;
        const origRowY = dragging! * props.itemHeight;

        const diff = touchY - origRowY;

        if (Math.abs(diff) / props.itemHeight >= swapThreshold) {
          const newI_ = dragging! + (diff > 0 ? 1 : -1);
          const newI = Math.max(0, Math.min(props.data.length - 1, newI_));
          console.log('I:', newI_, newI);

          LayoutAnimation.configureNext(
            LayoutAnimation.create(100, 'easeInEaseOut', 'opacity'),
          );
          if (dragging! !== newI) props.onSwapItems(dragging!, newI);
          setDragging(newI);
          setMidEvent(evt);
          setDraggingOfs(undefined);
        } else {
          setDraggingOfs(diff - props.itemHeight / 2);
        }
      },
      onVertScrollEnd: (_dy: number) => {
        LayoutAnimation.configureNext(
          LayoutAnimation.create(50, 'easeInEaseOut', 'opacity'),
        );
        initTouchY = 0;
        setDragging(null);
      },
    };
  }, [dragging, midEvent, props]);

  return (
    <FlatList
      ref={flatList}
      {...PanResponder(panHandlers).panHandlers}
      style={props.style}
      data={props.data}
      getItemLayout={getItemLayout}
      onLayout={onLayout}
      renderItem={(info: ListRenderItemInfo<I>) => (
        <TouchableWithoutFeedback
          delayLongPress={props.delayStartDrag || 150}
          onLongPress={() => onItemLongPress(info.index)}>
          <View
            style={
              dragging === info.index
                ? {
                    backgroundColor: '#FFFFFF4D',
                    top: draggingOfs,
                  }
                : null
            }>
            {props.renderItem(info)}
            <View
              style={{
                backgroundColor: '#00000000',
                ...(StyleSheet.absoluteFill as object),
              }}
            />
          </View>
        </TouchableWithoutFeedback>
      )}
    />
  );
}

export default DragList;
