import {processColor} from 'react-native';

export class Color {
  r: number;
  g: number;
  b: number;
  a: number;

  constructor(c: any) {
    const num = processColor(c);
    this.r = (num >> 16) & 255;
    this.g = (num >> 8) & 255;
    this.b = num & 255;
    this.a = ((num >> 24) & 255) / 255;
  }

  alpha(this, a: number) {
    this.a = a;
    return this;
  }

  string(this) {
    return `rgba(${this.r}, ${this.g}, ${this.b}, ${this.a})`;
  }
}

let Colors: any = {
  blue1: '#1C4087',
  blue2: '#4980EE',
  blue3: '#CCDDFF',

  purple1: '#AC87F8',
  purple2: '#C4A8FD',
  purple3: '#DDCCFF',
  purple4: '#F5F0FF',

  pink1: '#D62BD6',
  pink2: '#F887F8',
  pink3: '#FFCCFF',

  green: '#1FD85D',
  yellow: '#FFDE38',
  orange: '#FF612D',
  red: '#FF1663',

  gray1: '#242424',
  gray2: '#BBBBBB',
  gray3: '#EEEEEE',
};

Colors = {
  ...Colors,

  // Colors unrelated to the theme and that only need to be consistent within a
  // context.
  home: {
    dark1: 'rgba(0,0,0,0.35)',
    light1: 'rgba(255,255,255,0.7)',
  },
  planner: {
    semi1: new Color(Colors.purple4).alpha(0.3).string(),
    semi2: new Color(Colors.purple4).alpha(0.5).string(),
    dark1: 'rgba(0,0,0,0.7)',
    light1: 'rgba(255,255,255,0.7)',
  },
  camera: {
    light1: 'rgba(255,255,255,0.2)',
    dark1: 'rgba(0,0,0,0.7)',
    clear1: 'rgba(0,0,0,0)',
  },
  search: {
    dark1: 'rgba(0,0,0,0.5)',
    dark2: 'rgba(0,0,0,0.2)',
    clear1: 'rgba(0,0,0,0)',
  },
  profile: {
    light1: 'rgba(255,255,255,0.7)',
    dark1: 'rgba(0,0,0,0.5)',
    dark2: 'rgba(0,0,0,0.7)',
    clear1: 'rgba(0,0,0,0)',
  },
  player: {
    dark1: 'rgba(0,0,0,0.3)',
    dark2: 'rgba(0,0,0,0.7)',
    light1: 'rgba(255,255,255,0.7)',
    light2: 'rgba(255,255,255,0.2)',
    clear1: 'rgba(183,183,183,0)',
  },
  titles: {
    semi1: new Color(Colors.gray2).alpha(0.7).string(),
  },
  card: {
    semi1: new Color(Colors.gray2).alpha(0).string(),
    semi2: new Color(Colors.gray1).alpha(0.6).string(),
  },

  social: {
    snapchatYellow: '#FFFC00',
    youtubeRed: '#E80000',
  },
};
export default Colors;
