import {CompositeNavigationProp as CompNav} from '@react-navigation/native';
import {StackNavigationProp as StackNav} from '@react-navigation/stack';
import {MaterialTopTabNavigationProp as TabNav} from '@react-navigation/material-top-tabs';

export type RootStackParams = {
  Pager: undefined;
  Player: {vid: VidData};
  Profile: {profile: UserProfile; uploads: VidPreview[]};
};

export type HomePagerParams = {
  HomeStack: {screen: keyof HomeStackParams};
  Creation: undefined;
};

export type HomeStackParams = {
  Home: undefined;
  Search: undefined;
};

export type CreationStackParams = {
  Planner: undefined;
  Camera: {decls: Section.Decl[]; title: string};
  Publish: {data: Section.RecordData[]; title: string};
};

export type RootStackNav<S extends keyof RootStackParams> = StackNav<
  RootStackParams,
  S
>;
export type HomePagerNav<S extends keyof HomePagerParams> = CompNav<
  RootStackNav<'Pager'>,
  TabNav<HomePagerParams, S>
>;
export type HomeStackNav<S extends keyof HomeStackParams> = CompNav<
  HomePagerNav<'HomeStack'>,
  StackNav<HomeStackParams, S>
>;
export type CreationStackNav<S extends keyof CreationStackParams> = CompNav<
  HomePagerNav<'Creation'>,
  StackNav<CreationStackParams, S>
>;
