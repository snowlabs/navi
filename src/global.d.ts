declare namespace Section {
  interface Decl {
    key: string;
    title: string;
  }
  interface RecordData extends Decl {
    paths: string[];
  }
  interface Defn extends Decl {
    start: number;
    length: number;
  }
}

declare type SectionsList = Section.Defn[];

declare interface VidData {
  title: string;
  uri: string;
  thumbUri: string;
  length: number;
  sects: SectionsList;
}

declare interface VidPreview {
  title: string;
  thumbUri: string;
}

declare interface UserProfile {
  name: string; // Fname Lname
  tag: string; // @usertag
  location: string;
  bio: string;
  stars: string;
  avatarSrc: {};
  bannerSrc: {};
  socials: SocialLink[];
}

declare interface SocialLink {
  platform: 'snap' | 'yt';
  // TODO: prob need specific different data for each platform
  // for now its just urls
  url: string;
}
